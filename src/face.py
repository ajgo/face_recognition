"""
    人脸识别的主要逻辑
"""

import os

import cv2
import numpy as np
from PySide2.QtWidgets import QMessageBox

from config import *
import login
import search_information

face_recognizer = None


def _train_model(data_dir_path: str, model_path):
    if os.path.exists(data_dir_path) is False:
        os.mkdir(data_dir_path)
    faces, labels = prepare_training_data(data_dir_path)
    global face_recognizer
    face_recognizer = cv2.face.LBPHFaceRecognizer_create()
    face_recognizer.train(faces, np.array(labels))
    face_recognizer.save(model_path)


def train_person_model():
    _train_model(TRAIN_PERSON_DATA_PATH, PERSON_MODEL_PATH)


def train_user_model():
    _train_model(TRAIN_USER_DATA_PATH, USER_MODEL_PATH)


def _predict(image, model_path: str) -> int:
    test_image = image.copy()
    face, _ = detect_face(test_image)
    cv2.imshow('face', face)
    global face_recognizer
    face_recognizer = cv2.face.LBPHFaceRecognizer_create()
    face_recognizer.read(model_path)
    label = face_recognizer.predict(face)
    print(label[0])
    return label[0]


def predict_user(image) -> int:
    try:
        return _predict(image, USER_MODEL_PATH)
    except Exception:
        print("未检测到人脸信息")
        login_window = login.Login()
        QMessageBox.information(login_window.ui.btnFace, '提示', '未检测到人脸信息', QMessageBox.Yes)


def predict_person(image) -> int:
    try:
        return _predict(image, PERSON_MODEL_PATH)
    except Exception:
        print("未检测到人脸信息")
        search_information_window = search_information.Search()
        QMessageBox.warning(search_information_window.ui.btnStartSearch, '提示', '未检测到人脸信息', QMessageBox.Yes)


def detect_face(img):
    # 将测试图像转换为灰度图像，因为opencv人脸检测器需要灰度图像
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # 加载OpenCV人脸检测分类器
    face_cascade = cv2.CascadeClassifier(CLASSIFIER_PATH)

    # 检查多尺度图像，返回值是一张脸部区域信息的列表(x, y, width, height)
    faces = face_cascade.detectMultiScale(gray, scaleFactor=1.2, minNeighbors=5)

    # 如果未检出到面部，则返回原始图像
    if len(faces) == 0:
        return None, None

    # 目前假设只有一张脸，xy为左上角坐标，wh为矩阵的宽高
    (x, y, w, h) = faces[0]

    # 返回图像的正面部分
    return gray[y:y + w, x:x + h], faces[0]


def prepare_training_data(data_folder_path):
    # 获取数据文件夹中的目录（每个主题的一个目录）
    dirs = os.listdir(data_folder_path)

    # 两个列表分别保存所有的脸部和标签
    faces = []
    labels = []

    # 浏览每个目录并访问其中的图像
    for dir_name in dirs:
        # dir_name(str 类型)即标签
        label = int(dir_name)
        # 建立包含当前主题主题图像的目录路径
        subject_dir_path = f"{data_folder_path}/{dir_name}"
        # 获取给定主题目录内的图像名称
        subject_images_names = os.listdir(subject_dir_path)

        # 浏览每张图片并检测脸部，然后将脸部信息添加到脸部列表faces[]
        for image_name in subject_images_names:
            # 建立图像路径
            image_path = subject_dir_path + "/" + image_name
            # 读取图像
            image = cv2.imread(image_path)
            # 显示图像0.1s
            cv2.imshow("Training on image...", image)
            cv2.waitKey(10)

            # 检测脸部
            face, rect = detect_face(image)
            # 我们忽略未检测到的脸部
            if face is not None:
                # 将脸添加到脸部列表并添加相应的标签
                faces.append(face)
                labels.append(label)

    cv2.waitKey(1)
    cv2.destroyAllWindows()
    # 最终返回值为人脸和标签列表
    return faces, labels
