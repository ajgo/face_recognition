"""
    登入页面视图
"""
import sys

from PySide2.QtCore import QFile
from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import QApplication, QPushButton, QMessageBox

import handle_data
from face import *
from config import context
from register import Register
from search_information import Search


class Login:
    def __init__(self):
        qfile_login = QFile("ui/login.ui")
        qfile_login.open(QFile.ReadOnly)
        qfile_login.close()
        self.ui = QUiLoader().load(qfile_login)
        self.ui.btnLogin.clicked.connect(self.login)
        self.ui.btnReset.clicked.connect(self.reset)
        self.ui.btnFace.clicked.connect(self.face_login)
        self.ui.btnRegister.clicked.connect(self.go_to_register)

    def login(self):
        account = self.ui.txtAccount.text()
        password = self.ui.txtPassword.text()
        try:
            user = handle_data.login(account, password)
        except handle_data.PasswordError:
            QMessageBox.warning(self.ui.btnLogin, '提示', '密码不正确', QMessageBox.Yes)
            pass
        except handle_data.UserNotFoundError:
            QMessageBox.warning(self.ui.btnLogin, '提示', '该用户不存在', QMessageBox.Yes)
            pass
        else:
            context.login_user = user
            self.search_window = Search()
            self.search_window.ui.show()
            self.ui.close()

    def reset(self):
        self.ui.txtAccount.clear()
        self.ui.txtPassword.clear()

    def go_to_register(self):
        # register_window = Register()
        # register_window.ui.show()
        # register_window.ui.exec_()
        # self.ui.hide()
        self.register_window = Register()
        self.register_window.ui.show()
        self.ui.close()

    @staticmethod
    def open_video() -> int:
        cap = cv2.VideoCapture(0)
        count = 0
        image = None
        while count < 1:
            count += 1
            _, image = cap.read()
            # cv2.imshow('image', image)
            if cv2.waitKey(100) == 27:
                break
        cap.release()
        user_id = predict_user(image)
        return user_id

    def face_login(self):
        label = self.open_video()
        if label is None:
            return
        context.login_user = handle_data.find_by_id(label)
        # TODO 跳转到信息检索页面
        self.search_window = Search()
        self.search_window.ui.show()
        self.ui.close()

# if __name__ == '__main__':
#     app = QApplication(sys.argv)
#     login_window = Login()
#     login_window.ui.show()
#     sys.exit(app.exec_())
