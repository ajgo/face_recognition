"""
    用户对象实体类
"""
from peewee import *
from config import USER_DATA_BASE_PATH

db = SqliteDatabase(USER_DATA_BASE_PATH)
if db.is_closed() is True:
    db.connect()


class BaseModel(Model):
    class Meta:
        database = db


class User(BaseModel):
    username = CharField(max_length=32)
    password = CharField(max_length=32)


class Person(BaseModel):
    name = CharField(max_length=20)
    age = IntegerField()
    phone = CharField(max_length=11)
    major = CharField(max_length=50)
    nation = CharField(max_length=50)
    sex = CharField(max_length=2)
    hometown = CharField()
    occupation = CharField()


if __name__ == '__main__':
    db.create_tables([User])
    db.create_tables([Person])
