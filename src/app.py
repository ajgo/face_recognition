"""
    应用程序启动入口
"""
import sys

from PySide2.QtWidgets import QApplication

from login import Login

if __name__ == "__main__":
    app=QApplication(sys.argv)
    login_window = Login()
    login_window.ui.show()
    sys.exit(app.exec_())