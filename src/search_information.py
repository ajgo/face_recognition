import os
import sys
from concurrent.futures.thread import ThreadPoolExecutor

import cv2
from PySide2.QtCore import QFile
from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import QApplication, QTableWidgetItem

import config
import handle_data
from user_information import Person
import face

executor = ThreadPoolExecutor(max_workers=2)


class Search:
    def __init__(self):
        qfile_search = QFile("ui/search_information.ui")
        qfile_search.open(QFile.ReadOnly)
        qfile_search.close()

        self.ui = QUiLoader().load(qfile_search)
        self.ui.btnFace.isDefault = False
        self.ui.btnSave.clicked.connect(self.info_save)
        self.ui.btnReset.clicked.connect(self.info_reset)
        self.ui.btnFace.clicked.connect(self.info_face)
        self.ui.btnStartSearch.clicked.connect(self.start_search)
        self.ui.btnFace.setEnabled(False)
        self.ui.lblUserName.setText('用户名：{}'.format(str(config.context.login_user.username)))

    def info_save(self):
        person = Person()
        person.name = self.ui.txtName.text()
        person.age = self.ui.txtAge.text()
        person.occupation = self.ui.txtOccupation.text()
        person.phone = self.ui.txtPhone.text()
        person.major = self.ui.txtMajor.text()
        person.hometown = self.ui.txtHometown.text()
        person.nation = self.ui.txtNation.text()
        person.sex = self.ui.txtSex.text()
        # 调用插入数据方法
        face.context.current_person = handle_data.submit_information(person)
        # todo 提示成功或失败
        self.ui.btnFace.setEnabled(True)

    def info_reset(self):
        self.ui.txtName.clear()
        self.ui.txtAge.clear()
        self.ui.txtOccupation.clear()
        self.ui.txtHometown.clear()
        self.ui.txtMajor.clear()
        self.ui.txtNation.clear()
        self.ui.txtSex.clear()
        pass

    def info_face(self):
        """
            收集被检索人的信息
        """
        cap = cv2.VideoCapture(0)
        count = 0
        person = face.context.current_person
        person_path = f"{config.TRAIN_PERSON_DATA_PATH}/{person.id}"
        if os.path.exists(person_path) is False:
            os.makedirs(person_path)
        while count < 100:
            count += 1
            _, image = cap.read()
            path = f"{config.TRAIN_PERSON_DATA_PATH}/{person.id}/{count}.jpg"
            cv2.imwrite(path, image)
            cv2.imshow('Colletcing Images', image)
            if cv2.waitKey(10) == 27:
                break
        cv2.destroyWindow('Colletcing Images')
        cap.release()
        executor.submit(face.train_person_model())

    def start_search(self):
        cap = cv2.VideoCapture(0)
        count = 0
        image = None
        while count < 1:
            count += 1
            _, image = cap.read()
            if cv2.waitKey(100) == 27:
                break
        cap.release()
        person_id = face.predict_person(image)
        person = handle_data.search_information(person_id)
        self.display_info(person)

    def display_info(self, person: Person):
        # todo 展示用户信息
        person_info = handle_data.search_information(person.get_id())
        self.ui.tableWidget.setItem(0,1,QTableWidgetItem(person_info.name))
        self.ui.tableWidget.setItem(1,1,QTableWidgetItem(str(person_info.age)))
        self.ui.tableWidget.setItem(2,1,QTableWidgetItem(person_info.occupation))
        self.ui.tableWidget.setItem(3,1,QTableWidgetItem(person_info.phone))
        self.ui.tableWidget.setItem(4,1,QTableWidgetItem(person_info.major))
        self.ui.tableWidget.setItem(5,1,QTableWidgetItem(person_info.hometown))
        self.ui.tableWidget.setItem(6,1,QTableWidgetItem(person_info.nation))
        self.ui.tableWidget.setItem(7,1,QTableWidgetItem(person_info.sex))

        print(person.get_id())
        pass



