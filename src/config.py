"""
    用户程序的路径配置
"""

MODEL_PATH = "../model"
TRAIN_USER_DATA_PATH = "../train_data/user"
TRAIN_PERSON_DATA_PATH = "../train_data/person"
CLASSIFIER_PATH = "../model/haarcascade_frontalface_default.xml"
USER_MODEL_PATH = "../model/user.xml"
PERSON_MODEL_PATH = "../model/person.xml"
USER_DATA_BASE_PATH = "../user.db"


class Context:
    def __init__(self):
        self.login_user = None
        self.current_person = None
        self.register_user = None


context = Context()
