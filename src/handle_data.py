"""
    数据操作功能，主要用户处理 user.db
"""

from user_information import User, Person, DoesNotExist
import uuid


class UserNotFoundError(Exception):
    def __init__(self, *args, **kwargs):
        pass


class PasswordError(Exception):
    def __init__(self, *args, **kwargs):
        pass


class PasswordNotEqualError(Exception):
    def __init__(self, *args, **kwargs):
        pass


class UsernameDuplicateError(Exception):
    def __init__(self, *args, **kwargs):
        pass


def login(username: str, password: str) -> User:
    try:
        user: User = User().get(User.username == username)
    except DoesNotExist:
        raise UserNotFoundError()
    else:
        if user.password != password:
            raise PasswordError()
    return user


def register(username, password, confirm_password) -> User:
    if confirm_password != password:
        raise PasswordNotEqualError()
    user = None
    try:
        user = User.get(User.username == username)
    except DoesNotExist:
        pass

    if user is not None:
        raise UsernameDuplicateError()
    user = User().create(username=username, password=password)
    user.save()
    return user


def submit_information(person: Person) -> Person:
    person.uid = str(uuid.uuid4())
    person.save()
    return person


def find_by_id(user_id: int) -> User:
    return User().get(User.id == user_id)


def search_information(person_id: int) -> Person:
    return Person().get(Person.id == person_id)
