"""
    注册页面视图
"""
import os

import cv2

from PySide2.QtCore import QFile
from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import QApplication, QMessageBox

import handle_data
import login
from config import *
import config
from face import train_user_model
from search_information import executor


class Register:

    def __init__(self):
        qfile_register = QFile("ui/register.ui")
        qfile_register.open(QFile.ReadOnly)
        qfile_register.close()

        self.ui = QUiLoader().load(qfile_register)
        self.ui.btnRegister.clicked.connect(self.register)
        self.ui.btnReset.clicked.connect(self.reset)
        self.ui.btnBack.clicked.connect(self.back_to_login)

    def register(self):
        if self.ui.txtAccount.text() == '':
            QMessageBox.warning(self.ui.btnRegister, '提示', '账号不能为空', QMessageBox.Yes)

        if self.ui.txtPassword.text() == '':
            QMessageBox.warning(self.ui.btnRegister, "提示", "密码不能为空", QMessageBox.Yes)

        if self.ui.txtAccount.text() == '':
            QMessageBox.warning(self.ui.btnRegister, "提示", "确认密码不能为空",
                                QMessageBox.Yes)

        account = self.ui.txtAccount.text()
        password = self.ui.txtPassword.text()
        password_again = self.ui.txtPasswordAgain.text()

        if password != password_again:
            QMessageBox.warning(self.ui.btnRegister, "警告", "密码与确认密码不一致",
                                QMessageBox.Yes)
            return

        user = handle_data.register(account, password, password_again)
        QMessageBox.information(self.ui.btnRegister, '提示', '注册成功！准别开始录入人脸信息！', QMessageBox.Yes)
        context.register_user = user
        self.collect_user_face()

    def collect_user_face(self):
        """
            收集注册人的信息
        """
        cap = cv2.VideoCapture(0)
        count = 0
        user = context.register_user
        user_path = f"{config.TRAIN_USER_DATA_PATH}/{user.id}"
        if os.path.exists(user_path) is False:
            os.makedirs(user_path)
        while count < 100:
            count += 1
            _, image = cap.read()
            path = f"{config.TRAIN_USER_DATA_PATH}/{user.id}/{count}.jpg"
            cv2.imwrite(path, image)
            cv2.imshow('Collecting Images', image)
            if cv2.waitKey(100) == 27:
                break
        cv2.destroyWindow('Collecting Images')
        cap.release()
        executor.submit(train_user_model())

    def reset(self):
        self.ui.txtAccount.clear()
        self.ui.txtPassword.clear()
        self.ui.txtPasswordAgain.clear()

    def back_to_login(self):
        self.login_window = login.Login()
        self.login_window.ui.show()
        self.ui.close()

# if __name__ == '__main__':
#     app = QApplication([])
#     register = Register()
#     register.ui.show()
#     app.exec_()
